<?php
/**
 * Plugin Name: ClusterPress Doc
 * Plugin URI: https://cluster.press
 * Description: Utilisation des pages de découverte d'un site pour permettre aux membres et adeptes de contribuer à la documentation de ClusterPress.
 * Version: 1.0.0-beta2
 * License: GPLv2 or later (license.txt)
 * Author: imath
 * Author URI: https://imathi.eu/
 * Text Domain: clusterpress-doc
 * Domain Path: /languages/
 * Network: True
 * Default Language: fr_FR
 * cluster_id: doc
 * cluster_name: Documentation ClusterPress
 * cluster_icon: img/doc.svg
 * cluster_requires: 4.7
 */

// Exit if accessed directly
defined( 'ABSPATH' ) || exit;

/**
 * Add the addon's dir to the available ones.
 *
 * @since 1.0.0
 *
 * @param string  $dir     The absolute path to the Cluster's dir.
 * @param string  $cluster The expected Cluster ID.
 * @return string          The absolute path to the Cluster's dir.
 */
function cpd_set_doc_cluster_dir( $dir = '', $cluster = '' ) {
	// Only set this addon's dir if the Doc Cluster is loading.
	if ( empty( $cluster ) || 'doc' !== $cluster ) {
		return $dir;
	}

	return plugin_dir_path( __FILE__ );
}
add_filter( 'cp_custom_cluster_includes_dir', 'cpd_set_doc_cluster_dir', 10, 2 );
