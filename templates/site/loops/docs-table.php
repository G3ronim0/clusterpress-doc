<?php
/**
 * ClusterPress Docs Edit table.
 *
 * @package ClusterPress Doc\templates\site\loop
 * @subpackage docs-table
 *
 * @since 1.0.0
 */
?>

<form class="cp-form" method="post" action="" data-cp-role="filters">

	<div class="cp-table-actions">

		<div class="cp-search-area cp-form-section">
			<label for="cp-form-search" class="screen-reader-text"><?php cp_search_placehoder(); ?></label>
			<input type="search" placeholder="<?php cp_search_placehoder(); ?>" name="cp_filters[cp_search]" id="cp-form-search">

			<button type="submit">
				<span class="dashicons dashicons-search"></span>
				<span class="screen-reader-text"><?php cp_search_placehoder(); ?></span>
			</button>
		</div>

		<?php if ( cp_filter_has_options( 'doc_status' ) ) : ?>

			<div class="cp-filter-area cp-form-section">
				<label for="cp-form-filter" class="screen-reader-text"><?php cp_filter_placehoder(); ?></label>
				<select name="cp_filters[doc_status]" id="cp-form-filter">

					<?php cp_filter_options( 'doc_status' ); ?>

				</select>

				<button type="submit">
					<span class="dashicons dashicons-filter"></span>
					<span class="screen-reader-text"><?php cp_filter_placehoder(); ?></span>
				</button>
			</div>

		<?php endif ; ?>

	</div>

</form><!-- // .cp-form -->

<?php if ( cp_doc_site_has_ressources() ) : ?>

	<div class="cp-pagination top">

		<div class="cp-total-count">

			<?php cp_site_posts_total_count(); ?>

		</div>

		<?php if ( cp_site_posts_has_pagination_links() ) : ?>

			<div class="cp-pagination-links">

				<?php cp_site_posts_pagination_links(); ?>

			</div>

		<?php endif ; ?>

	</div>

	<table class="doc-table">

		<?php cp_doc_table_headers() ;?>

		<tbody>

			<?php while ( cp_site_the_posts() ) : cp_site_the_post() ; ?>

				<tr class="doc">

					<td class="cp-doc">

						<h3 class="doc-title">

							<?php if ( cp_doc_site_is_published() ) : ?>

								<a href="<?php cp_site_the_post_permalink(); ?>"><?php cp_site_the_title(); ?></a>

							<?php else : ?>

								<?php cp_site_the_title(); ?>

							<?php endif ;?>

						</h3>

						<div class="doc-actions">

							<?php cp_doc_site_edit_actions(); ?>

						</div>

					</td>

					<td class="doc-author">

						<?php cp_site_the_post_author(); ?>

					</td>

					<td class="doc-date">

						<?php cp_site_the_post_date(); ?>

					</td>

				</tr><!-- // .user -->

			<?php endwhile ; ?>

		</tbody>

	</table><!-- // .doc-table -->

	<?php if ( cp_site_posts_has_pagination_links() ) : ?>

		<div class="cp-pagination bottom">

			<div class="cp-pagination-links">

				<?php cp_site_posts_pagination_links(); ?>

			</div>

		</div>

	<?php endif ; ?>

<?php else :

	cp_site_no_posts_found();

endif;
