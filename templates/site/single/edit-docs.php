<?php
/**
 * ClusterPress Doc Site's Edit documentation template.
 *
 * @package ClusterPress Doc\templates\site\single
 * @subpackage edit-docs
 *
 * @since 1.0.0
 */
?>

<h2><?php cp_doc_site_loop_title(); ?></h2>

<?php if ( cp_doc_is_form() ) : ?>

	<?php cp_get_template_part( 'site/single/doc-form' ) ; ?>

<?php else : ?>

	<div id="cp-doc-sites" class="sites archive">

		<?php cp_get_template_part( 'site/loops/docs-table' ) ; ?>

	</div>

<?php endif; ?>
