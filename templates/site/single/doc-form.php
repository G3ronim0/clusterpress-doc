<?php
/**
 * ClusterPress Doc Site's form documentation template.
 *
 * @package ClusterPress Doc\templates\site\single
 * @subpackage doc-form
 *
 * @since 1.0.0
 */
?>

<div id="editing-doc">

	<?php cp_doc_is_currently_edited(); ?>

</div>

<form class="cp-form cp-edit-doc" method="post" action="<?php cp_doc_site_get_form_action(); ?>" data-cp-role="edit-doc">

	<?php cp_doc_set_form_objects(); ?>

	<?php if ( cp_doc_is_form_new() ) : ?>

		<input type="text" name="cp_doc[post_title]" id="cp-doc-title" value="<?php cp_doc_to_edit_title(); ?>">

	<?php else : ?>

		<input type="text" name="cp_doc[post_title]" id="cp-doc-title" value="<?php cp_doc_to_edit_title(); ?>" class="cp-doc-exists">
		<?php cp_doc_to_edit_link(); ?>

	<?php endif; ?>

	<label for="cp-doc-title"><?php cp_doc_to_edit_title_label(); ?></label>

	<?php cp_doc_to_edit_content(); ?>
	<label for="cp-doc-content"><?php cp_doc_to_edit_content_label(); ?></label>


	<fieldset id="cp-doc-chapters" class="categories">
		<legend><?php esc_html_e( 'Chapitre', 'clusterpress-doc' ); ?></legend>

		<?php cp_doc_to_edit_chapters(); ?>

	</fieldset>

	<div class="cp-doc-publishing-actions">

		<div class="cp-doc-status">

			<select name="cp_doc[post_status]" id="cp-doc-status">

				<?php cp_doc_to_edit_status(); ?>

			</select>
			<label for="cp-doc-status"><?php cp_doc_to_edit_status_label(); ?></label>

		</div>

		<div class="submit">

			<?php cp_doc_to_edit_submit() ;?>

		</div>
	</div>

	<?php cp_doc_reset_form_objects(); ?>

</form>
