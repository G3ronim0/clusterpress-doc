<?php
/**
 * ClusterPress Doc Table of content template.
 *
 * @package ClusterPress Doc\templates\doc\loops
 * @subpackage toc
 *
 * @since 1.0.0
 */

if ( cp_doc_has_toc() ) : ?>

	<ul class="documentation-toc">

		<?php while ( cp_doc_the_toc_items() ) : cp_doc_the_toc_item() ; ?>

			<li class="toc-item">

				<?php if ( cp_doc_toc_item_has_link() ) : ?>

					<h3><a href="<?php cp_doc_toc_item_link(); ?>"><?php cp_doc_toc_item_name(); ?></a></h3>

				<?php else : ?>

					<h3><?php cp_doc_toc_item_name(); ?></h3>

					<?php if ( cp_doc_toc_item_has_description() ) : ?>

						<p><?php cp_doc_toc_item_description(); ?></p>

					<?php endif ; ?>

				<?php endif ; ?>

				<?php if ( cp_doc_toc_item_has_content() ) : ?>

					<ul><?php cp_doc_toc_item_content(); ?></ul>

				<?php endif; ?>

			</li><!-- // .toc-item -->

		<?php endwhile ; ?>

	</ul><!-- // .documentation-toc -->

<?php else :

	cp_doc_toc_no_items_found();

endif;
