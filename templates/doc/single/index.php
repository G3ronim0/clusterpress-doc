<?php
/**
 * ClusterPress Doc Index root template.
 *
 * @package ClusterPress Doc\templates\doc\single
 * @subpackage index
 *
 * @since 1.0.0
 */

cp_doc_feedbacks(); ?>

<div class="cp-content">

	<div id="cp-doc-top"></div>

	<?php if ( cp_current_doc_has_toc() ) : ?>

		<div class="cp-doc-toc-container">

			<h3><?php esc_html_e( 'Table des matières.', 'clusterpress-doc' ); ?></h3>

			<?php cp_doc_the_doc_toc(); ?>

		</div>

	<?php endif; ?>

	<div class="cp-doc-content">

		<?php cp_doc_the_content(); ?>

	</div>

	<div class="cp-doc-credits">
		<h6><?php esc_html_e( 'Crédits', 'clusterpress-doc' ); ?></h6>

		<ul class="doc-contributors">

			<?php cp_doc_the_contributors(); ?>

		</ul>

	</div>

	<ul class="cp-doc-navigation">

		<li class="previous">
			<?php cp_doc_previous_link(); ?>
		</li>

		<li class="toc">
			<?php cp_doc_back_to_toc_link(); ?>
		</li>

		<li class="next">
			<?php cp_doc_next_link(); ?>
		</li>

	</ul>

	<?php cp_doc_the_edit_link(); ?>

</div>

<?php
/**
 * This is used to reset the WordPress Post global.
 *
 * Make sure to leave it into your custom templates.
 */
cp_doc_reset_post_object(); ?>
