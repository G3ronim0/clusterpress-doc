<?php
/**
 * Admin functions
 *
 * @since  1.0.0
 *
 * @package ClusterPress Doc\doc
 * @subpackage admin
 */

// Exit if accessed directly
defined( 'ABSPATH' ) || exit;

function cp_doc_admin_print_inline_style() {
	wp_add_inline_style( 'common', '
		#adminmenu #menu-posts-cp_doc .wp-menu-image img {
			width: 18px;
			padding: 6px 0 0 0;
		}
	' );
}
add_action( 'cp_admin_enqueue_scripts', 'cp_doc_admin_print_inline_style' );

/**
 * Edit the post preview link for the admin context.
 *
 * @since  1.0.0
 *
 * @param  string  $preview_link The doc item preview link.
 * @param  WP_Post $post         The doc item Object.
 * @return string                The doc item preview link.
 */
function cp_doc_preview_post_link( $preview_link = '', $post = null ) {
	if ( ! is_admin() || wp_doing_ajax() || empty( $post ) || 'cp_doc' !== get_post_type( $post ) ) {
		return $preview_link;
	}

	return add_query_arg( array(
		'doc_preview_id'    => $post->ID,
		'doc_preview_nonce' => wp_create_nonce( 'cp_doc_preview_' . $post->ID ),
	), $preview_link );
}
add_filter( 'preview_post_link', 'cp_doc_preview_post_link', 10, 2 );
