<?php
/**
 * Doc's Table of content loop.
 *
 * @package ClusterPress Doc\doc\classes
 * @subpackage doc-toc-items-loop
 *
 * @since 1.0.0
 */

// Exit if accessed directly
defined( 'ABSPATH' ) || exit;

/**
 * Docs loop Class.
 *
 * @since 1.0.0
 */
class CP_Doc_Toc_Items_Loop extends CP_Cluster_Loop {

	/**
	 * Constructor
	 *
	 * @since 1.0.0
	 *
	 * @param  array $args the loop args
	 */
	public function __construct( $args = array() ) {
		$r = wp_parse_args( $args, array(
			'parent'  => 0,
		) );

		$toc_items = array();
		$tax_query = array();

		if ( ! empty( $r['parent'] ) ) {
			$tax_query[] = array(
				'taxonomy'         => 'cp_chapter',
				'field'            => 'term_id',
				'terms'            => (int) $r['parent'],
				'include_children' => false,
			);

			if ( cp_current_taxonomy() ) {
				$toc_items = array_merge( array( cp_current_taxonomy() ), $toc_items );
			}
		} else {
			$toc_items = cp_doc_get_chapters( $r );
		}

		// Set the table of content content!
		if ( ! empty( $toc_items ) ) {
			foreach ( $toc_items as $kti => $toc_item ) {
				$toc_items[ $kti ]->content = '';

				if ( $tax_query ) {
					$docs = get_posts( array(
						'post_type'   => 'cp_doc',
						'tax_query'   => $tax_query,
						'numberposts' => -1,
						'order'       => 'ASC',
						'orderby'     => 'title',
					) );

					if ( ! empty( $docs ) ) {
						$toc_items[ $kti ]->content .= sprintf( '<li class="cp-doc">%s<ul>', sprintf( __( '%s Ressources', 'clusterpress-doc' ), '<span class="dashicons dashicons-media-default"></span>' ) );

						foreach ( $docs as $doc ) {
							$toc_items[ $kti ]->content .= sprintf( '<li class="cp-doc-%1$s"><a href="%2$s">%3$s</a></li>',
								(int) $doc->ID,
								esc_url( get_post_permalink( $doc->ID ) ),
								esc_html( $doc->post_title )
							);
						}

						$toc_items[ $kti ]->content .= '</ul></li>';
					}
				}

				$toc_items[ $kti ]->content .= wp_list_categories( array(
					'child_of'         => $toc_item->term_id,
					'taxonomy'         => 'cp_chapter',
					'hide_empty'       => 0,
					'echo'             => 0,
					'show_option_none' => __( 'Plus aucun sous-chapitres.', 'clusterpress-doc' ),
					'title_li'         => sprintf( __( '%s Chapitres', 'clusterpress-doc' ), '<span class="dashicons dashicons-category"></span>' ),
					'show_count'       => 1,
				) );
			}
		}

		parent::start( array(
			'plugin_prefix'    => 'cp_doc',
			'item_name'        => 'toc_item',
			'item_name_plural' => 'toc_items',
			'items'            => $toc_items,
			'total_item_count' => count( $toc_items ),
			'page'             => 1,
			'per_page'         => 1,
		) );
	}
}
