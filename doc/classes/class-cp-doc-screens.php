<?php
/**
 * Doc Screens Manager.
 *
 * @package ClusterPress Doc\doc\classes
 * @subpackage doc-screens
 *
 * @since 1.0.0
 */

// Exit if accessed directly
defined( 'ABSPATH' ) || exit;

/**
 * Replace the content by the Doc templates
 *
 * @since  1.0.0
 */
class CP_Doc_Screens {

	/**
	 * The constructor
	 *
	 * Only filter the content once wp_head was fired.
	 *
	 * @since  1.0.0
	 */
	public function __construct() {
		if ( doing_action( 'wp_head' ) || ( ! cp_is_doc() && ! cp_is_docs() ) ) {
			return;
		}

		add_filter( 'cp_the_content', array( $this, 'doc_content' ), 10, 1 );
	}

	/**
	 * Launch the Doc's Screens manager.
	 *
	 * @since 1.0.0
	 *
	 * @return CP_Doc_Screen $screens The instance of the class.
	 */
	public static function start() {
		$cp = clusterpress();

		if ( empty( $cp->doc->screens ) ) {
			$cp->doc->screens = new self();
		}

		return $cp->doc->screens;
	}

	/**
	 * Buffer the Doc's root template.
	 *
	 * @since  1.0.0
	 *
	 * @param  string $content The Post content.
	 * @return string          The ClusterPress Doc's content.
	 */
	public function doc_content( $content = '' ) {
		$template_part = 'doc/archive/index';

		if ( cp_is_doc() ) {
			$template_part = 'doc/single/index';
		}

		add_filter( 'cp_template_stack', 'cp_doc_set_template_stack_entries', 20, 1 );

		$buffer = cp_buffer_template_part( $template_part, null, false );

		remove_filter( 'cp_template_stack', 'cp_doc_set_template_stack_entries', 20, 1 );

		return $buffer;
	}
}
add_action( 'cp_do_templates', array( 'CP_Doc_Screens', 'start' ), 10 );
