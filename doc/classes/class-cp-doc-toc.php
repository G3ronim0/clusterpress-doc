<?php
/**
 * Doc's single item TOC.
 *
 * @package ClusterPress Doc\doc\classes
 * @subpackage doc-toc
 *
 * @since 1.0.0
 */

// Exit if accessed directly
defined( 'ABSPATH' ) || exit;

/**
 * Builds the Table of content and parse doc's post content to add anchors
 *
 * This part is using @mattyza's WP Section Index scripts for content parsing
 * @see http://wordpress.org/plugins/wp-section-index/
 *
 * @since 1.0.0
 */
class CP_Doc_Toc {

	public $headings;
	public $content;
	public $toc;
	public $increment;
	public $backtotop_counter;

	/**
	 * The Constructor
	 *
	 * @since 1.0.0
	 *
	 * @param string $content the content to parse and to build the toc for?
	 */
	public function __construct( $content = '' ) {

		if ( empty( $content) ) {
			return new WP_Error( 'missing_parameter' );
		}

		$this->content  = $content;
		$this->headings = cp_doc_get_headings();

		$this->create_content_anchors();

		if ( cp_doc_is_backtotops_enabled() ) {
			$this->create_back_to_tops();
		}

		$this->create_toc();
	}

	/**
	 * Get anchors name for a match
	 *
	 * @since 1.0.0
	 *
	 * @param string $match the matched content
	 */
	private function get_anchors( $match ) {

		if ( empty( $match ) ) {
			return;
		}

		// Setup the text to be used as the anchor.
		$anchor_text = '';
		$anchor_text = strtolower( $match );

		// Limit anchor text to only 4 words.
		$anchor_text_bits = explode( ' ', $anchor_text );

		if ( count( $anchor_text_bits ) > 4 ) {
			$limited_anchor_text_bits = 4;
		} else {
			$limited_anchor_text_bits = count( $anchor_text_bits );
		}

		$anchor_text = '';
		for ( $i = 0; $i < $limited_anchor_text_bits; $i++ ) {
			$anchor_text .= $anchor_text_bits[ $i ];

			if ( $i !== $limited_anchor_text_bits - 1 ) {
				$anchor_text .= ' ';
			}
		}

		// Setup and clean up anchor text.
		$anchor_text = remove_accents( $anchor_text );
		$anchor_text = wptexturize( $anchor_text );
		$anchor_text = str_replace( ' ', '_', $anchor_text );
		$anchor_text = str_replace( '.', '', $anchor_text );
		$anchor_text = rawurlencode( $anchor_text );
		$anchor_text = $anchor_text .'_'. $this->increment;

		$this->increment += 1;

		return $anchor_text;
	}

	/**
	 * Add anchors to content that matched
	 *
	 * @since 1.0.0
	 *
	 * @param array $matches the matched array
	 */
	private function get_section_headings( $matches ) {

		$anchor_text = $this->get_anchors( $matches[1] );

		$anchor = '';

		$anchor = '<a name="' . $anchor_text . '" id="' . $anchor_text . '" class="doc_anchor">&nbsp;</a>';

		// Construct the filtered heading tag with the newly created anchor.
		$filtered_heading = '';
		$filtered_heading = "\n" . $anchor . "\n" . $matches[0];

		return trim( $filtered_heading );
	}

	/**
	 * Loops the activated headings and creates the anchors
	 *
	 * @since 1.0.0
	 */
	public function create_content_anchors() {

		$this->increment = 0;

		foreach( $this->headings as $heading ) {

			$pattern = '/<' . $heading . '[^>]*>(.*?)<\/' . $heading . '>/i';

			$this->content = preg_replace_callback( $pattern, array( &$this, 'get_section_headings' ), $this->content );
		}
	}

	/**
	 * Gets back to top links
	 *
	 * @since 1.0.0
	 *
	 * @param array $matches the matched content
	 */
	public function get_back_to_tops( $matches ) {

		$backtotop_anchor = '';

		if ( $this->backtotop_counter !== 0 ) {

			// Setup the text to be used as the anchor.
			$backtotop_anchor = apply_filters( 'cp_doc_backtotop_anchor', '<a href="#cp-doc-top" class="back-to-top">' . __( 'Back to top', 'clusterpress-doc' ) . ' &uarr;</a>' );
		}

		$this->backtotop_counter++;

		// Construct the filtered heading tag with the newly created anchor.
		$hyperlink = '';
		$hyperlink = "\n" . $backtotop_anchor . "\n" . $matches[0];

		$hyperlink = trim( $hyperlink );

		return $hyperlink;
	}

	/**
	 * Loop activated headings to add back to top anchors to content
	 *
	 * @since 1.0.0
	 */
	public function create_back_to_tops() {

		foreach( $this->headings as $heading ) {

			$pattern = '/<' . $heading . '[^>]*>(.*?)<\/' . $heading . '>/i';

			$this->content = preg_replace_callback( $pattern, array( &$this, 'get_back_to_tops' ), $this->content );
		}

		// Let's finally add one more back to top anchor at the end of the content
		$this->content .= "\n" . apply_filters( 'cp_doc_backtotop_anchor', '<a href="#cp-doc-top" class="back-to-top">' . __( 'Back to top', 'clusterpress-doc' ) . ' &uarr;</a>' ) . "\n";

	}

	/**
	 * Creates the table of content
	 *
	 * @since 1.0.0
	 */
	public function create_toc() {

		$found_headings = $this->toc = array();
		$this->increment = 0;


		foreach( $this->headings as $heading ) {

			$pattern = '/<' . $heading . '[^>]*>(.*?)<\/' . $heading . '>/i';

			preg_match_all( $pattern, $this->content, $matches, PREG_OFFSET_CAPTURE );

			foreach( $matches[1] as $key => $ids ) {
				$found_headings[$ids[1]]= array( 'type' => $heading, 'anchor' => $this->get_anchors( $ids[0] ), 'title' => $ids[0] );
			}

		}

		ksort( $found_headings );

		$this->toc = $found_headings;
	}
}
