<?php
/**
 * Doc Tags.
 *
 * @package ClusterPress Doc\doc
 * @subpackage tags
 *
 * @since 1.0.0
 */

// Exit if accessed directly
defined( 'ABSPATH' ) || exit;

/**
 * Add filters to the Site Posts loop.
 *
 * @since  1.0.0
 */
function cp_doc_add_site_posts_filters() {
	add_filter( 'cp_site_get_loop_global',       'cp_doc_site_posts_loop_global', 10, 1 );
	add_filter( 'cp_site_has_posts_args',        'cp_doc_has_posts_args',         10, 1 );
	add_filter( 'cp_site_get_no_posts_found',    'cp_doc_get_no_docs_found',      10, 1 );
	add_filter( 'cp_site_get_posts_total_count', 'cp_doc_get_docs_total_count',   10, 3 );
	add_filter( 'cp_site_get_post_actions',      'cp_doc_get_doc_actions',        10, 1 );
}

/**
 * Remove the filters from the Site Posts loop.
 *
 * @since  1.0.0
 */
function cp_doc_remove_site_posts_filters() {
	remove_filter( 'cp_site_get_loop_global',       'cp_doc_site_posts_loop_global', 10, 1 );
	remove_filter( 'cp_site_has_posts_args',        'cp_doc_has_posts_args',         10, 1 );
	remove_filter( 'cp_site_get_no_posts_found',    'cp_doc_get_no_docs_found',      10, 1 );
	remove_filter( 'cp_site_get_posts_total_count', 'cp_doc_get_docs_total_count',   10, 3 );
	remove_filter( 'cp_site_get_post_actions',      'cp_doc_get_doc_actions',        10, 1 );
}

/**
 * Init a Table of content's loop.
 *
 * @since  1.0.0
 *
 * @param  int $parent The parent chapter to list chapters/ressources for.
 * @return bool        True if there are toc items to display. False otherwise.
 */
function cp_doc_has_toc( $parent = null ) {
	$cp        = clusterpress();
	$term_args = array();

	if ( ! $parent ) {
		$parent_term = cp_current_taxonomy();

		if ( ! empty ( $parent_term->term_id ) ) {
			$term_args['parent'] = $parent_term->term_id;
		}
	} else {
		$term_args['parent'] = (int) $parent;
	}

	/**
	 * Filter here if you need to edit the loop args.
	 *
	 * @since  1.0.0
	 *
	 * @param  array  $term_args The loop arguments.
	 */
	$cp->doc->toc_loop = new CP_Doc_Toc_Items_Loop( apply_filters( 'cp_doc_has_toc_args', $term_args ) );

	/**
	 * Filter here if you need to edit the has_items() result.
	 *
	 * @since  1.0.0
	 *
	 * @param  bool  $value True if there are toc items to display. False otherwise.
	 * @param  array $r     The loop arguments.
	 */
	return apply_filters( 'cp_doc_has_toc', $cp->doc->toc_loop->has_items(), $term_args );
}

/**
 * Whether there are more toc items available in the loop to iterate on.
 *
 * @since 1.0.0
 *
 * @return object The Toc Item object.
 */
function cp_doc_the_toc_items() {
	return clusterpress()->doc->toc_loop->items();
}

/**
 * Load the current toc item into the loop.
 *
 * @since 1.0.0
 *
 * @return object The current Toc Item Object.
 */
function cp_doc_the_toc_item() {
	return clusterpress()->doc->toc_loop->the_item();
}

/**
 * Get the current toc item of the loop.
 *
 * @since  1.0.0
 *
 * @return object The current Toc Item Object.
 */
function cp_doc_get_current_toc_item() {
	$cp = clusterpress();

	if ( empty( $cp->doc->toc_loop->toc_item ) ) {
		return null;
	}

	/**
	 * Filter here if you need to edit the current toc item.
	 *
	 * @since  1.0.0
	 *
	 * @param object The current Toc Item Object.
	 */
	return apply_filters( 'cp_doc_get_current_toc_item', $cp->doc->toc_loop->toc_item );
}

/**
 * Do the current toc item has a link ?
 *
 * @since  1.0.0
 *
 * @return bool True if the current toc item has a link. False otherwise.
 */
function cp_doc_toc_item_has_link() {
	$current_item = cp_doc_get_current_toc_item();

	/**
	 * Filter here if you need to edit returned value.
	 *
	 * @since  1.0.0
	 *
	 * @param bool True if the item has an url. False otherwise.
	 */
	return (bool) apply_filters( 'cp_doc_toc_item_has_link', ! empty( $current_item->url ) );
}

/**
 * Output the toc item link.
 *
 * @since  1.0.0
 *
 * @return string HTML Output.
 */
function cp_doc_toc_item_link() {
	echo esc_url( cp_doc_toc_get_item_link() );
}

	/**
	 * Get the toc item link.
	 *
	 * @since  1.0.0
	 *
	 * @return string the toc item link.
	 */
	function cp_doc_toc_get_item_link() {
		$current_item = cp_doc_get_current_toc_item();

		if ( ! $current_item ) {
			return '';
		}

		/**
		 * Filter here if you need to edit the item link.
		 *
		 * @since  1.0.0
		 *
		 * @param string The item link.
		 */
		return apply_filters( 'cp_doc_toc_get_item_link', $current_item->url );
	}

/**
 * Output the toc item name.
 *
 * @since  1.0.0
 *
 * @return string HTML Output.
 */
function cp_doc_toc_item_name() {
	echo esc_html( cp_doc_toc_get_item_name() );
}

	/**
	 * Get the toc item name.
	 *
	 * @since  1.0.0
	 *
	 * @return string the toc item name.
	 */
	function cp_doc_toc_get_item_name() {
		$current_item = cp_doc_get_current_toc_item();

		if ( ! $current_item ) {
			return '';
		}

		/**
		 * Filter here if you need to edit the item name.
		 *
		 * @since  1.0.0
		 *
		 * @param string The item name.
		 */
		return apply_filters( 'cp_doc_toc_get_item_name', $current_item->name );
	}

/**
 * Do the current toc item has a description ?
 *
 * @since  1.0.0
 *
 * @return bool True if the current toc item has a description. False otherwise.
 */
function cp_doc_toc_item_has_description() {
	$current_item = cp_doc_get_current_toc_item();

	/**
	 * Filter here if you need to edit returned value.
	 *
	 * @since  1.0.0
	 *
	 * @param bool True if the item has a description. False otherwise.
	 */
	return (bool) apply_filters( 'cp_doc_toc_item_has_description', ! empty( $current_item->description ) );
}

/**
 * Output the toc item description.
 *
 * @since  1.0.0
 *
 * @return string HTML Output.
 */
function cp_doc_toc_item_description() {
	echo esc_html( cp_doc_toc_get_item_description() );
}

	/**
	 * Get the toc item description.
	 *
	 * @since  1.0.0
	 *
	 * @return string the toc item description.
	 */
	function cp_doc_toc_get_item_description() {
		$current_item = cp_doc_get_current_toc_item();

		if ( ! $current_item ) {
			return '';
		}

		/**
		 * Filter here if you need to edit the item description.
		 *
		 * @since  1.0.0
		 *
		 * @param string The item description.
		 */
		return apply_filters( 'cp_doc_toc_get_item_description', $current_item->description );
	}

/**
 * Do the current toc item has content ?
 *
 * @since  1.0.0
 *
 * @return bool True if the current toc item has content. False otherwise.
 */
function cp_doc_toc_item_has_content() {
	$current_item = cp_doc_get_current_toc_item();

	/**
	 * Filter here if you need to edit returned value.
	 *
	 * @since  1.0.0
	 *
	 * @param bool True if the item has content. False otherwise.
	 */
	return (bool) apply_filters( 'cp_doc_toc_item_has_content', ! empty( $current_item->content ) );
}

/**
 * Output the toc item content.
 *
 * @since  1.0.0
 *
 * @return string HTML Output.
 */
function cp_doc_toc_item_content() {
	echo wp_kses_post( cp_doc_toc_get_item_content() );
}

	/**
	 * Get the toc item content.
	 *
	 * @since  1.0.0
	 *
	 * @return string the toc item content.
	 */
	function cp_doc_toc_get_item_content() {
		$current_item = cp_doc_get_current_toc_item();

		if ( ! $current_item ) {
			return '';
		}

		/**
		 * Filter here if you need to edit the item content.
		 *
		 * @since  1.0.0
		 *
		 * @param string The item content.
		 */
		return apply_filters( 'cp_doc_toc_get_item_content', $current_item->content );
	}

/**
 * Remove the TOC lis containing no chapters.
 *
 * @since 1.0.0
 *
 * @return string JS Output.
 */
function cp_doc_chapters_none_fix() {
	return '
		( function($) {
			$.each( $( \'.cp_chapter .cat-item-none\' ), function( i, none ) {
				$( none ).closest( \'.cp_chapter\' ).remove();
			} );
		} )( jQuery );
	';
}

/**
 * Output the "no toc items found" feedback.
 *
 * @since  1.0.0
 *
 * @return string HTML Output.
 */
function cp_doc_toc_no_items_found() {
	echo cp_doc_toc_get_no_items_found();
}

	/**
	 * Gett the "no toc items found" feedback.
	 *
	 * @since  1.0.0
	 *
	 * @return string The "no toc items found" feedback.
	 */
	function cp_doc_toc_get_no_items_found() {
		$message = __( 'Aucune table des matière n\'a été trouvée.', 'clusterpress-doc' );

		clusterpress()->feedbacks->add_feedback(
			'doc[empty-toc]',
			/**
			 * Filter here to edit the "no toc items found" feedback.
			 *
			 * @since  1.0.0
			 *
			 * @param  string $message The "no toc items found" feedback.
			 */
			apply_filters( 'cp_doc_toc_get_no_items_found', $message ),
			'info'
		);

		cp_get_template_part( 'assets/feedbacks' );
	}

/**
 * Get the displayed doc object.
 *
 * @since  1.0.0
 *
 * @return WP_Post The displayed doc object.
 */
function cp_doc_get_current_doc() {
	global $post;
	$cp_doc = clusterpress()->doc;

	// Used to reset the post global once we don't need it anymore.
	$cp_doc->reset_single_post = $post;


	$current_doc = cp_doc_displayed_item();

	if ( empty( $current_doc->ID ) ) {
		return null;
	}

	if ( 'cp_doc' !== $post->post_type ) {
		$post = get_post( $current_doc );

		// Setup the Postdata.
		setup_postdata( $post );
	}

	/**
	 * Filter here to edit the displayed doc object.
	 *
	 * @since  1.0.0
	 *
	 * @param  WP_Post $post The displayed doc object.
	 */
	return apply_filters( 'cp_doc_get_current_doc', $post );
}

/**
 * Output Specific Feedback to the displayed doc (preview info).
 *
 * @since  1.0.0
 *
 * @return string HTML Output.
 */
function cp_doc_feedbacks() {
	// Make sure to display ClusterPress feedbacks first.
	cp_feedbacks();

	if ( cp_is_doc_preview() ) {
		$message = sprintf( __( 'Vous prévisualisez : %s', 'clusterpress-doc' ), get_the_title( cp_doc_get_current_doc() ) );

		clusterpress()->feedbacks->add_feedback(
			'doc[is_preview]',
			/**
			 * Filter here to edit the "preview" feedback.
			 *
			 * @since  1.0.0
			 *
			 * @param  string $message The "preview" feedback.
			 */
			apply_filters( 'cp_doc_toc_get_no_items_found', $message ),
			'info'
		);

		cp_get_template_part( 'assets/feedbacks' );
	}
}

/**
 * Output the Doc's content
 *
 * @since  1.0.0
 *
 * @return string HTML Output.
 */
function cp_doc_the_content() {
	echo cp_doc_get_content();
}

	/**
	 * Get the doc's content.
	 *
	 * @since  1.0.0
	 *
	 * @return string The doc's content.
	 */
	function cp_doc_get_content() {
		$doc = cp_doc_get_current_doc();

		if ( is_null( $doc ) ) {
			return;
		}

		/**
		 * Filter here to edit the doc's content.
		 *
		 * @since  1.0.0
		 *
		 * @param string  $post_content The doc's content.
		 * @param WP_Post $doc          The displayed doc object.
		 */
		return apply_filters( 'cp_doc_get_content', $doc->post_content, $doc );
	}

/**
 * Check whether a doc has a table of contents.
 *
 * @since  1.0.0
 *
 * @return bool True if the current doc has a toc. False otherwise.
 */
function cp_current_doc_has_toc() {
	$doc = cp_doc_get_current_doc();

	return apply_filters( 'cp_current_doc_has_toc', ! empty( $doc->toc->toc ) );
}

/**
 * Output the Doc's table of contents
 *
 * @since  1.0.0
 *
 * @return string HTML Output.
 */
function cp_doc_the_doc_toc() {
	echo wp_kses( cp_doc_get_doc_toc(), array(
		'a'  => array( 'href' => true, 'class' => true, 'title' => true ),
		'dl' => array( 'class' => true ),
		'dt' => array( 'class' => true ),
	) );
}

	/**
	 * Get the doc's table of contents.
	 *
	 * @since  1.0.0
	 *
	 * @return string The doc's table of contents.
	 */
	function cp_doc_get_doc_toc() {
		$doc = cp_doc_get_current_doc();

		if ( empty( $doc->toc->toc ) || ! is_a( $doc->toc, 'CP_Doc_Toc' ) ) {
			return;
		}

		$doc_link = trailingslashit( get_post_permalink( $doc->ID ) );
		$output = '<dl class="cp-doc-toc">';

		foreach( $doc->toc->toc as $heading ) {

			$output .= sprintf( '
				<dt class="cp-doc-toc-element dt-%1$s">
					<a href="%2$s" title="%3$s">%4$s</a>
				</dt>
				',
				sanitize_key( $heading['type'] ),
				esc_url( $doc_link . '#' . $heading['anchor'] ),
				esc_attr( $heading['title'] ),
				esc_html( $heading['title'] )
			);
		}

		$output .= '</dl>';

		/**
		 * Filter here to edit the doc's table of contents.
		 *
		 * @since  1.0.0
		 *
		 * @param string  $output The doc's table of contents.
		 * @param WP_Post $doc    The displayed doc object.
		 */
		return apply_filters( 'cp_doc_get_doc_toc', $output, $doc );
	}

/**
 * Output the list of contributors.
 *
 * @since  1.0.0
 *
 * @return string HTML Output.
 */
function cp_doc_the_contributors() {
	echo wp_kses( cp_doc_get_contributors(), array(
		'li'  => true,
		'a'   => array( 'href' => true, 'class' => true, 'title' => true ),
		'img' => array( 'src' => true, 'class' => true, 'height' => true, 'width' => true ),
	) );
}

	/**
	 * Get the list of contributors.
	 *
	 * @since  1.0.0
	 *
	 * @return string The list of contributors output.
	 */
	function cp_doc_get_contributors() {
		$doc = cp_doc_get_current_doc();

		if ( empty( $doc->post_author ) ) {
			return;
		}

		$contributors = array( $doc->post_author );
		$revisions    = wp_get_post_revisions( $doc );

		if ( $revisions ) {
			$contributors = array_merge( wp_list_pluck( $revisions, 'post_author' ), $contributors );
		}

		$users = cp_get_users( array( 'count_total' => false, 'include' => wp_parse_id_list( $contributors ) ) );

		$lis = array();
		foreach ( $users as $user ) {
			$user = new WP_User( $user );

			$lis[] = sprintf( '<li><a href="%1$s" title="%2$s">%3$s</a></li>',
				esc_url( cp_user_get_url( array(), $user ) ),
				esc_attr( $user->display_name ),
				cp_get_user_avatar( array( 'width' => 60, 'height' => 60, 'user_id' => $user->ID ) )
			);
		}

		/**
		 * Filter here to edit the list of contributors.
		 *
		 * @param  string $value The list of contributors output.
		 * @param  array  $users The list of users.
		 */
		return apply_filters( 'cp_doc_get_contributors', join( "\n", $lis ), $users );
	}

/**
 * Display the edit link to "power" users!
 *
 * @since  1.0.0
 *
 * @return string HTML Output.
 */
function cp_doc_the_edit_link() {
	if ( cp_is_doc_preview() ) {
		return;
	}

	$link = cp_doc_get_edit_link();

	if ( ! $link ) {
		return;
	}

	echo wp_kses( $link, array(
		'p'    => true,
		'a'    => array( 'href' => true, 'class' => true ),
		'span' => array( 'class' => true ),
	) );
}

	/**
	 * Get the doc's edit link.
	 *
	 * @since  1.0.0
	 *
	 * @return string The doc's edit link.
	 */
	function cp_doc_get_edit_link() {
		$doc = cp_doc_get_current_doc();

		if ( empty( $doc->ID ) ) {
			return false;
		}

		if ( current_user_can( 'edit_cp_docs' ) ) {
			$url = get_edit_post_link( $doc );
		} elseif ( cp_cluster_is_enabled( 'site' ) && current_user_can( 'edit_cp_doc', array( 'site_id' => get_current_blog_id() ) ) ) {
			$site = cp_get_site_by( 'id', get_current_blog_id() );

			if ( ! empty( $site->blog_id ) ) {
				$url  = add_query_arg( cp_doc_get_site_action( 'edit' ), $doc->ID, cp_doc_get_site_url( $site ) );
			}
		}

		if ( empty( $url ) ) {
			return false;
		}

		/**
		 * Filter here to edit the doc's edit link.
		 *
		 * @since  1.0.0
		 *
		 * @param  string  $value The doc's edit link.
		 * @param  WP_Post $doc   The doc object.
		 */
		return apply_filters( 'cp_doc_get_edit_link', sprintf( '
			<p>
				<a href="%1$s" class="doc-edit-link">
					<span class="dashicons dashicons-edit"></span>
					%2$s
				</a>
			</p>
			',
			$url,
			esc_html__( 'Modifier la ressource', 'clusterpress-doc' )
		), $doc );
	}

/**
 * Output the link to the previous doc.
 *
 * @since 1.0.0
 *
 * @return string HTML Output.
 */
function cp_doc_previous_link() {
	echo cp_doc_get_previous_link();
}

	/**
	 * Get the link to the previous doc.
	 *
	 * @since 1.0.0
	 *
	 * @return string The link to the previous doc.
	 */
	function cp_doc_get_previous_link() {

		add_filter( 'get_previous_post_sort', 'cp_doc_adjacent_sort_previous' );

		$previous = '&nbsp;' . get_previous_post_link( '&laquo; %link', '%title', true, '', 'cp_chapter' );

		remove_filter( 'get_previous_post_sort', 'cp_doc_adjacent_sort_previous' );

		/**
		 * Filter here to edit the link to the previous doc.
		 *
		 * @since 1.0.0
		 *
		 * @param string $previous The link to the previous doc.
		 */
		return apply_filters( 'cp_doc_get_previous_link', $previous );
	}

/**
 * Output the link to the next doc.
 *
 * @since 1.0.0
 *
 * @return string HTML Output.
 */
function cp_doc_next_link() {
	echo cp_doc_get_next_link();
}

	/**
	 * Get the link to the next doc.
	 *
	 * @since 1.0.0
	 *
	 * @return string The link to the next doc.
	 */
	function cp_doc_get_next_link() {

		add_filter( 'get_next_post_sort', 'cp_doc_adjacent_sort_next' );

		$next = '&nbsp;' . get_next_post_link( '%link &raquo;', '%title', true, '', 'cp_chapter' );

		remove_filter( 'get_next_post_sort', 'cp_doc_adjacent_sort_next' );

		/**
		 * Filter here to edit the link to the next doc.
		 *
		 * @since 1.0.0
		 *
		 * @param string $next The link to the next doc.
		 */
		return apply_filters( 'cp_doc_get_next_link', $next );
	}

/**
 * Output links to Table of contents.
 *
 * @since 1.0.0
 *
 * @return string HTML Output.
 */
function cp_doc_back_to_toc_link() {
	echo cp_doc_get_back_to_toc_link();
}

	/**
	 * Get the links to Table of contents.
	 *
	 * @since 1.0.0
	 *
	 * @return string The links to Table of contents.
	 */
	function cp_doc_get_back_to_toc_link() {
		$toc_link         = array();
		$chapter_toc_link = get_the_taxonomies( cp_doc_get_current_doc() );

		if ( ! empty( $chapter_toc_link['cp_chapter'] ) ) {
			$chapter    = get_taxonomy( 'cp_chapter' );
			$replace    = __( 'Revenir au sommaire du chapitre', 'clusterpress-doc' );
			$toc_link[] = str_replace( $chapter->label, $replace, $chapter_toc_link['cp_chapter'] );
		}

		$toc_link[] = sprintf(
			'<a href="%1$s">%2$s</a>',
			esc_url( get_post_type_archive_link( 'cp_doc' ) ),
			esc_html__( 'Revenir au sommaire général', 'clusterpress-doc' )
		);

		if ( count( $toc_link ) > 1 ) {
			$toc_link = '<ul class="toc-links"><li>' . join( '</li><li>', $toc_link ) . '</li></ul>';
		} else {
			$toc_link = reset( $toc_link );
		}

		/**
		 * Filter here to edit the links to Table of contents.
		 *
		 * @since 1.0.0
		 *
		 * @param string $toc_link The to Table of contents.
		 */
		return apply_filters( 'cp_doc_get_back_to_toc_link', $toc_link );
	}

/**
 * Reset the WP Post global
 *
 * @since 1.0.0
 */
function cp_doc_reset_post_object() {
	global $post;
	$cp_doc = clusterpress()->doc;

	if ( ! empty( $cp_doc->reset_single_post ) ) {
		$post = $cp_doc->reset_single_post;

		// Reset the post data
		wp_reset_postdata();
	}
}
