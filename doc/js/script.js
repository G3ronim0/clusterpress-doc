/* global cp, wp, clusterPress */
window.cp = window.cp || {};
window.wp = window.wp || {};

( function( exports, $ ) {

	// Bail if not set
	if ( typeof clusterPress === 'undefined' ) {
		return;
	}

	/**
	 * Doc Class
	 * @type {Object}
	 */
	cp.Doc = {
		/**
		 * Listen to the dom and act accordingly!
		 * @return {void}
		 */
		start: function() {
			this.heartbeat = wp.heartbeat || {};

			$( document).ready( this.setHeartBeat.bind( this ) );

			$( '.doc-actions' ).on( 'click', '.trash', this.confirmTrash );

			if ( 1 === clusterPress.docSettings.cb ) {
				$( '#cp-doc-chapters :checkbox' ).on( 'click', this.radioBehavior );
			}

			$( document ).on( 'heartbeat-send.clusterpress-doc', this.heartbeatSend );
			$( document ).on( 'heartbeat-tick.clusterpress-doc', this.heartbeatTick );
		},

		confirmTrash: function( event ) {

			if ( false === window.confirm( clusterPress.strings.cpDocConfirm ) ) {
				event.preventDefault();
				return;
			}

			return event;
		},

		radioBehavior: function( event ) {
			$.each( $( '#cp-doc-chapters :checked' ), function( cb, checkbox ) {
				if ( $( checkbox ).prop( 'id' ) !== $( event.target ).prop( 'id' ) ) {
					$( checkbox ).prop( 'checked', false );
				}
			} );
		},

		setHeartBeat: function() {
			if ( typeof clusterPress.docSettings.pulse === 'undefined' || ! this.heartbeat ) {
				return;
			}

			this.heartbeat.interval( Number( clusterPress.docSettings.pulse ) );

			$.fn.extend( {
				'heartbeat-send': function() {
					return this.bind( 'heartbeat-send.clusterpress-doc' );
				}
			} );

			$.fn.extend( {
				'heartbeat-tick': function() {
					return this.bind( 'heartbeat-tick.clusterpress-doc' );
				}
			} );
		},

		heartbeatSend: function( event, data ) {
			if ( ! $( '#cp_doc_id' ).length || ! $( '#cp_doc_id' ).val() ) {
				return event;
			}

			data.cp_doc_id  = $( '#cp_doc_id' ).val();
			data.cp_site_id = $( '#cp_doc_id' ).data( 'cp-site' );
		},

		heartbeatTick: function( event, data ) {
			var isLocked = $( '#editing-doc' ).find( '.locked' );

			if ( ! data.cp_doc_lock ) {
				if ( isLocked.length ) {
					isLocked.remove();
					$( '#editing-doc' ).hide();
					$( '.cp-doc-publishing-actions' ).show();
				}

			} else if ( ! isLocked.length ) {
				$( '#editing-doc' ).show().find( '.cp-feedback-message' ).prepend( data.avatar );
				$( '.cp-doc-publishing-actions' ).hide();
			}
		}
	};

	// Launch ClusterPress Doc
	cp.Doc.start();

} )( cp, jQuery );
