<?php
/**
 * Actions
 *
 * @since  1.0.0
 *
 * @package ClusterPress Doc\doc
 * @subpackage actions
 */

// Exit if accessed directly
defined( 'ABSPATH' ) || exit;

/**
 * Register the Doc section inside Sites discovery pages.
 *
 * @since  1.0.0
 */
function cp_doc_register_site_section() {
	// Registers The Site's discovery Documentation section
	cp_register_cluster_section( 'site', array(
		'id'               => 'site-doc',
		'name'             => cp_doc_get_post_type_archive_title(),
		'cluster_id'       => 'site',
		'slug'             => cp_doc_get_post_type_archive_slug(),
		'rewrite_id'       => 'cp_site_doc',
		'template'         => 'site/single/edit-docs',
		'template_dir'     => trailingslashit( cp_doc_get_plugin_dir() ) . 'templates',
		'toolbar_items'    => array(
			'position'     => 60,
			'dashicon'     => 'dashicons-book-alt',
			'capability'   => 'cp_doc_edit_documentation',
		),
	) );
}
add_action( 'cp_register_cluster_sections', 'cp_doc_register_site_section', 15 );

/**
 * Unregister the CP Doc post type on site's discovery page.
 *
 * Used at the end of doc loops.
 *
 * @since 1.0.0
 */
function cp_doc_unregister_site_post_type() {
	unregister_post_type( 'cp_doc' );

	remove_action( 'cp_post_loop_end', 'cp_doc_unregister_site_post_type' );
}

/**
 * Register the CP Doc post type on site's discovery page.
 *
 * Used when startin a new doc loops.
 *
 * @since 1.0.0
 */
function cp_doc_register_site_post_type() {
	if ( cp_is_site_doc() && ! cp_current_sub_action() ) {
		cp_doc_register_post_type();

		add_action( 'cp_post_loop_end', 'cp_doc_unregister_site_post_type' );
	}
}
add_action( 'cp_post_loop_start', 'cp_doc_register_site_post_type' );

/**
 * Saves a doc into the database (Add or Edit).
 *
 * @since 1.0.0
 */
function cp_doc_save_doc() {
	if ( ! cp_doc_is_form() || empty( $_POST['cp_doc'] ) ) {
		return;
	}

	check_admin_referer( 'cp_doc_save_doc' );

	if ( ! current_user_can( 'publish_cp_docs' ) ) {
		cp_feedback_redirect( 'doc-not-allowed', 'error' );
	}

	$doc = wp_parse_args( array_diff_key( $_POST['cp_doc'], array( 'submit' => false ) ), array(
		'post_title'   => '',
		'post_content' => '',
		'post_status'  => 'draft',
		'post_type'    => 'cp_doc',
	) );

	// Give a feedback to the user before interrupting the process.
	if ( empty( $doc['post_title'] ) || empty( $doc['post_content'] ) ) {
		$_SERVER['REQUEST_URI'] = add_query_arg( 'error', 'site[doc-missing]' );
		return;
	}

	// Attach terms to the doc.
	if ( isset( $_POST['tax_input']['cp_chapter'] ) ) {
		$tax_input = $_POST['tax_input'];

		if ( count( $tax_input['cp_chapter'] ) > 1 ) {
			$tax_input['cp_chapter'] = array_filter( $tax_input['cp_chapter'] );
		}

		$doc['tax_input'] = $tax_input;
	}

	// Default to a generic error.
	$feedbacks = array( 'error' => 'doc-error', 'success' => 'doc-inserted' );

	switch_to_blog( cp_get_displayed_site_id() );

	// Make sure the post type and the taxonomy are set.
	cp_doc_register_post_type();
	cp_doc_register_taxonomy();

	// Update the existing doc
	if ( ! empty( $doc['ID'] ) ) {
		// Get the previous post status
		$post_status = get_post_status( $doc['ID'] );

		// Set the default updated status.
		$feedbacks['success'] = 'doc-updated';

		if ( 'publish' !== $post_status && 'publish' === $doc['post_status'] ) {
			$feedbacks['success'] = 'doc-published';
		}

		// Updated the post
		$doc_id = wp_update_post( $doc );

	// Insert The new doc.
	} else {
		// Set the default inserted status.
		$feedbacks['success'] = 'doc-inserted';

		if ( 'publish' === $doc['post_status'] ) {
			$feedbacks['success'] = 'doc-published';
		}

		$doc_id = wp_insert_post( $doc );
	}

	// Reset globals.
	unregister_post_type( 'cp_doc' );
	unregister_taxonomy( 'cp_chapter' );

	restore_current_blog();

	if ( is_wp_error( $doc_id ) ) {
		cp_feedback_redirect( $feedbacks['error'], 'error' );
	}

	// Redirect the user on the edit form.
	cp_feedback_redirect( $feedbacks['success'], 'updated', add_query_arg(
		cp_doc_get_site_action( 'edit' ),
		$doc_id,
		cp_doc_get_site_url()
	) );
}
add_action( 'cp_page_actions', 'cp_doc_save_doc' );

/**
 * Deletes a doc from the database.
 *
 * @since 1.0.0.
 */
function cp_doc_delete_doc() {
	if ( ! cp_is_site_doc() || cp_doc_is_form() ) {
		return;
	}

	$delete_action = cp_doc_get_site_action( 'trash' );

	if ( empty( $_GET[ $delete_action ] ) ) {
		return;
	}

	check_admin_referer( 'cp_doc_trash_doc' );

	// Set the redirect URL
	$redirect = cp_doc_get_site_url();

	switch_to_blog( cp_get_displayed_site_id() );

	$doc = get_post( (int) $_GET[ $delete_action ] );

	if ( empty( $doc->ID ) ) {
		cp_feedback_redirect( 'doc-error', 'error', $redirect );
	}

	if ( ! current_user_can( 'delete_cp_doc', $doc ) ) {
		cp_feedback_redirect( 'doc-not-allowed', 'error', $redirect );
	}

	if ( ! wp_trash_post( $doc ) ) {
		cp_feedback_redirect( 'doc-error', 'error', $redirect );
	}

	restore_current_blog();

	cp_feedback_redirect( 'doc-deleted', 'updated', $redirect );
}
add_action( 'cp_page_actions', 'cp_doc_delete_doc', 12 );

/**
 * When displaying the form to edit the doc, first check it is
 * not being edited by someone else.
 *
 * @since  1.0.0
 */
function cp_doc_screen_post_lock() {
	if ( ! cp_doc_is_form_edit() ) {
		return;
	}

	$action  = cp_doc_get_site_action( 'edit' );
	$site_id = cp_get_displayed_site_id();

	if ( $site_id ) {
		switch_to_blog( $site_id );

		if ( cp_doc_check_post_lock( $_GET[ $action ] ) ) {
			cp_feedback_redirect( 'editing-doc-alt', 'error', cp_doc_get_site_url() );
		}

		restore_current_blog();
	}
}
add_action( 'cp_page_actions', 'cp_doc_screen_post_lock', 15 );

/**
 * Register the JavaScript for the Doc Addon.
 *
 * @since  1.0.0
 *
 * @param  string $url The url to the ClusterPress JS folder (not used).
 * @param  string $min Whether to load the minified version of the script.
 */
function cp_doc_register_scripts( $url = '', $min = '' ) {

	wp_register_script(
		'clusterpress-doc',
		sprintf( '%1$sdoc/js/script%2$s.js', trailingslashit( cp_doc_get_plugin_url() ), $min ),
		array( 'clusterpress-common', 'heartbeat' ),
		cp_doc_get_addon_version(),
		true
	);
}
add_action( 'cp_register_scripts', 'cp_doc_register_scripts', 10, 2 );
