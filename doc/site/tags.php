<?php
/**
 * Doc's Site Tags.
 *
 * @package ClusterPress Doc\doc\site
 * @subpackage tags
 *
 * @since 1.0.0
 */

// Exit if accessed directly
defined( 'ABSPATH' ) || exit;

/**
 * Wrapper of the Site's Cluster Posts loop init functions
 *
 * @since 1.0.0.
 *
 * @return bool True if docs were found. False otherwise.
 */
function cp_doc_site_has_ressources() {
	$ressource_args = array(
		'post_type'   => 'cp_doc',
		'post_status' => cp_doc_get_editable_stati(),
		'filter'      => cp_get_displayed_site_id(),
	);

	$filter_vars = cp_get_filter_var();

	if ( ! empty( $filter_vars['doc_status'] ) && 'all' !== $filter_vars['doc_status'] ) {
		$ressource_args['post_status'] = $filter_vars['doc_status'];
	}

	return cp_site_has_posts( $ressource_args );
}

/**
 * Output the doc table headers.
 *
 * @since  1.0.0
 *
 * @return string HTML Output.
 */
function cp_doc_table_headers() {
	echo cp_doc_get_table_headers();
}
	/**
	 * Get the doc table headers.
	 *
	 * @since 1.0.0
	 *
	 * @param  bool $use_foot True to use the tfoot tag. False otherwise.
	 * @return string The doc table headers.
	 */
	function cp_doc_get_table_headers( $use_foot = true ) {
		$output = '';

		$headers = array(
			'doc'    => esc_html__( 'Titre de la Ressource', 'clusterpress-doc' ),
			'author' => esc_html__( 'Auteur',                'clusterpress-doc' ),
			'date'   => esc_html__( 'Date',                  'clusterpress-doc' ),
		);

		$header_cells = '';
		foreach ( $headers as $kh => $vh ) {
			$header_cells .= sprintf( '<th>%s</th>', $vh ) . "\n";
		}

		$output = sprintf( "<thead>\n\t<tr>%s</tr>\n</thead>", $header_cells );

		if ( true === $use_foot ) {
			$output .= sprintf( "\n<tfoot>\n\t<tr>%s</tr>\n</tfoot>", $header_cells );
		}

		/**
		 * Filter here to edit the doc table headers.
		 *
		 * @since 1.0.0
		 *
		 * @param string  $output   The doc table headers.
		 * @param array   $headers  The column headers.
		 * @param bool    $use_foot True to use the tfoot tag. False otherwise.
		 */
		return apply_filters( 'cp_doc_get_table_headers', $output, $headers, $use_foot );
	}

/**
 * Output the title of the Documentation site's discovery page.
 *
 * @since 1.0.0
 *
 * @return string HTML Output.
 */
function cp_doc_site_loop_title() {
	$doc_title = cp_doc_get_post_type_archive_title();

	if ( current_user_can( 'publish_cp_docs', cp_get_displayed_site_id() ) && ! cp_doc_is_form_new() ) {
		$doc_title .= "\n" . sprintf( '
			<button class="button cp-doc-new">
				<a href="%1$s">
					<span class="dashicons dashicons-plus"></span>
					<span class="screen-reader-text">%2$s</span>
				</a>
			</button>
			',
			esc_url( cp_doc_get_add_link() ),
			esc_html__( 'Ajouter une ressource', 'clusterpress-doc' )
		);
	}

	/**
	 * Filter here to edit the Doc section's title.
	 *
	 * @since  1.0.0
	 *
	 * @param  string $value The Doc section's title.
	 */
	echo apply_filters( 'cp_doc_site_loop_title', wp_kses( $doc_title, array(
		'button' => array( 'class' => true ),
		'a'      => array( 'href' => true, 'class' => true ),
		'span'   => array( 'class' => true ),
	) ) );
}

/**
 * Check if a doc is published.
 *
 * @since 1.0.0
 *
 * @return bool True if the doc is published. False otherwise.
 */
function cp_doc_site_is_published() {
	if ( ! cp_doc_is_form() ) {
		$post = cp_site_get_current_post();
	} else {
		$post = (object) cp_get_doc_to_edit();
	}

	if ( empty( $post->post_status ) ) {
		return false;
	}

	return 'publish' === $post->post_status;
}

/**
 * Output Actions for the current doc in the loop.
 *
 * @since 1.0.0
 *
 * @return string HTML Output.
 */
function cp_doc_site_edit_actions() {
	$actions = cp_doc_site_get_edit_actions();

	if ( ! $actions ) {
		return;
	}

	echo wp_kses( $actions, array(
		'a'    => array( 'href' => true, 'class' => true, 'target' => true ),
		'span' => array( 'class' => true ),
		'img'  => array( 'src' => true, 'height' => true, 'width' => true, 'alt' => true, 'class' => true ),
	) );
}

	/**
	 * Get the actions for the current doc in the loop.
	 *
	 * @since 1.0.0
	 *
	 * @return string Action links.
	 */
	function cp_doc_site_get_edit_actions() {
		$post = cp_site_get_current_post();

		if ( empty( $post->ID ) ) {
			return;
		}

		/**
		 * Filter here to add/edit/remove the doc actions
		 *
		 * @since 1.0.0
		 *
		 * @param array   $value The list of available actions.
		 * @param WP_Post $post  The current doc in the loop.
		 */
		$actions = apply_filters( 'cp_doc_site_get_edit_actions', cp_doc_get_site_action( 'all' ), $post );
		unset( $actions['add'] );

		if ( ! current_user_can( 'delete_cp_doc', $post ) ) {
			unset( $actions['trash'] );
		}

		if ( ! current_user_can( 'edit_cp_doc', array( 'site_id' => get_current_blog_id() ) ) ) {
			unset( $actions['edit'] );

		// Only add the Preview action to "power" users!
		} elseif ( 'draft' === $post->post_status ) {
			$actions['external'] = __( 'Previsualiser', 'clusterpress-doc' );
		}

		if ( empty( $actions ) ) {
			return false;
		}

		$output = array();

		foreach ( $actions as $ka => $action ) {
			if ( 'external' !== $ka ) {
				$target = '';
				$url = add_query_arg( $action, $post->ID, cp_doc_get_site_url() );

				if ( 'trash' === $ka ) {
					$url = wp_nonce_url( $url, 'cp_doc_trash_doc' );
				}
			} else {
				$target = ' target="_blank"';
				$url = get_preview_post_link( $post, array(
					'doc_preview_id'    => $post->ID,
					'doc_preview_nonce' => wp_create_nonce( 'cp_doc_preview_' . $post->ID ),
				) );
			}

			$output[ $ka ] = sprintf( '
				<a href="%1$s" class="%2$s"%3$s>
					<span class="dashicons dashicons-%2$s"></span>
					<span class="screen-reader-text">%4$s</span>
				</a>',
				esc_url( $url ),
				sanitize_html_class( $ka ),
				$target,
				esc_html( ucfirst( $action ) )
			);
		}

		if ( ! empty( $actions['edit'] ) ) {
			$user_editing = cp_doc_check_post_lock( $post->ID );

			if ( $user_editing ) {
				array_unshift( $output, sprintf( '<div class="locked"><span class="dashicons dashicons-lock"></span> %s</div>',
					cp_get_user_avatar( array(
						'user_id' => $user_editing,
						'width'   => 18,
						'height'  => 18,
					) )
				) );

				unset( $output[ 'edit'], $output[ 'trash'] );
			}
		}

		return join( ' | ', $output );
	}

/**
 * Get the Doc to edit.
 *
 * NB: the global is edited later to include the WP_Post properties
 * when editing a doc.
 *
 * @since  1.0.0
 *
 * @return array The Doc's parameters.
 */
function cp_get_doc_to_edit() {
	$cp = clusterpress();

	if ( ! isset( $cp->doc->edited_doc ) ) {
		$edit_action = cp_doc_get_site_action( 'edit' );

		if ( empty( $_GET[ $edit_action ] ) ) {
			return null;
		}

		$cp->doc->edited_doc = array();
		$cp->doc->edited_doc['ID'] = (int) $_GET[ $edit_action ];
	}

	return $cp->doc->edited_doc;
}

/**
 * Display a feedback to inform the doc is currently edited.
 *
 * @since  1.0.0
 *
 * @return string HTML Output
 */
function cp_doc_is_currently_edited() {
	clusterpress()->feedbacks->set_feedback(
		'site[editing-doc]',
		'error'
	);

	cp_get_template_part( 'assets/feedbacks' );
}

/**
 * Output the action attribute of the doc form.
 *
 * @since  1.0.0
 *
 * @return string HTML Output.
 */
function cp_doc_site_get_form_action() {
	$link = cp_doc_get_add_link();

	if ( cp_doc_is_form_edit() ) {
		$doc = cp_get_doc_to_edit();

		if ( ! empty( $doc['ID'] ) ) {
			$link = add_query_arg( cp_doc_get_site_action( 'edit' ), $doc['ID'], cp_doc_get_site_url() );
		}
	}

	echo esc_url( $link );
}

/**
 * Set the form objects.
 *
 * Switch the current blog, set the current edited doc and register the taxonomy.
 *
 * @since  1.0.0
 */
function cp_doc_set_form_objects() {
	$cp = clusterpress();

	switch_to_blog( cp_get_displayed_site_id() );

	$doc = cp_get_doc_to_edit();

	if ( ! empty( $doc['ID'] ) ) {
		$cp->doc->edited_doc = (array) get_post( $doc['ID'] );
	} else {
		$cp->doc->edited_doc = array();
	}

	if ( ! empty( $_POST['cp_doc'] ) ) {
		$cp->doc->edited_doc = array_merge( $_POST['cp_doc'], $cp->doc->edited_doc );
	}

	// Register the Post type and the Taxonomy.
	cp_doc_register_post_type();
	cp_doc_register_taxonomy();
}

/**
 * Reset the form objects.
 *
 * Restore the current blog, unset the doc and unregister the taxonomy.
 *
 * @since 1.0.0.
 */
function cp_doc_reset_form_objects() {
	$cp = clusterpress();

	restore_current_blog();

	if ( isset( $cp->doc->edited_doc ) ) {
		unset( $cp->doc->edited_doc );
	}

	unregister_taxonomy( 'cp_chapter' );
	unregister_post_type( 'cp_doc' );
}

/**
 * Output the Doc's title to edit.
 *
 * @since  1.0.0
 *
 * @return string HTML Output.
 */
function cp_doc_to_edit_title() {
	echo esc_attr( cp_doc_to_edit_get_title() );
}

	/**
	 * Get the Doc's title to edit.
	 *
	 * @since  1.0.0
	 *
	 * @return string The Doc's title to edit.
	 */
	function cp_doc_to_edit_get_title() {
		$doc   = cp_get_doc_to_edit();
		$title = '';

		if ( ! empty( $doc['post_title'] ) ) {
			$title = $doc['post_title'];
		}

		/**
		 * Filter here to edit the doc's title to edit.
		 *
		 * @since  1.0.0
		 *
		 * @param  string $title The doc's title to edit.
		 * @param  array  $doc   The doc parameters.
		 */
		return apply_filters( 'cp_doc_to_edit_get_title', $title, $doc );
	}

/**
 * Output the doc link for the edit form.
 *
 * @since  1.0.0
 *
 * @return string HTML Output.
 */
function cp_doc_to_edit_link() {
	echo wp_kses( cp_doc_to_edit_get_link(), array(
		'a' => array( 'href' => true, 'class' => true, 'target' => true ),
		'span' => array( 'class' => true ),
	) );
}

	/**
	 * Get the the doc link for the edit form.
	 *
	 * @since  1.0.0
	 *
	 * @return string The doc link for the edit form.
	 */
	function cp_doc_to_edit_get_link() {
		$doc = (object) cp_get_doc_to_edit();

		if ( empty( $doc->guid ) ) {
			return;
		}

		if ( cp_doc_site_is_published() ) {
			$button_args = array(
				'dashicon'           => 'visibility',
				'screen-reader-text' => __( 'Afficher la ressource', 'clusterpress-doc' ),
				'url'                => get_post_permalink( $doc ),
				'target'             => '',
			);
		} else {
			$button_args = array(
				'dashicon'           => 'external',
				'screen-reader-text' => __( 'Prévisualiser la ressource', 'clusterpress-doc' ),
				'url'                => get_preview_post_link( $doc, array(
					'doc_preview_id'    => $doc->ID,
					'doc_preview_nonce' => wp_create_nonce( 'cp_doc_preview_' . $doc->ID ),
				) ),
				'target'             => ' target="_blank"',
			);
		}

		/**
		 * Filter here to edit the doc link for the edit form.
		 *
		 * @since  1.0.0
		 *
		 * @param  string $value       The doc link for the edit form.
		 * @param  array  $button_args The link parts.
		 * @param  object $doc         The doc object.
		 */
		return apply_filters( 'cp_doc_to_edit_get_link', sprintf( '
			<a href="%1$s" class="doc-link %2$s"%3$s>
				<span class="dashicons dashicons-%2$s"></span>
				<span class="screen-reader-text">%4$s</span>
			</a>',
			esc_url( $button_args['url'] ),
			sanitize_html_class( $button_args['dashicon'] ),
			$button_args['target'],
			esc_html( $button_args['screen-reader-text'] )
		), $button_args, $doc );
	}

/**
 * Output the title label.
 *
 * @since  1.0.0
 *
 * @return string HTML Output.
 */
function cp_doc_to_edit_title_label() {
	echo esc_html( apply_filters( 'cp_doc_to_edit_title_label', __( 'Titre de la ressource', 'clusterpress-doc' ) ) );
}

/**
 * Output the Doc's content to edit.
 *
 * @since  1.0.0
 *
 * @return string HTML Output.
 */
function cp_doc_to_edit_content() {
	$editor_args = apply_filters( 'cp_doc_to_edit_content_editor_args', array(
		'textarea_name' => 'cp_doc[post_content]',
		'wpautop'       => true,
		'media_buttons' => false,
		'editor_class'  => 'cp-doc-tinymce',
		'textarea_rows' => get_option( 'default_post_edit_rows', 10 ),
		'teeny'         => false,
		'dfw'           => false,
		'tinymce'       => true,
		'quicktags'     => false
	) );

	// Temporarly filter the editor
	add_filter( 'mce_buttons',          'cp_doc_editor_button_filter', 10, 2 );
	add_filter( 'tiny_mce_before_init', 'cp_doc_editor_before_init',   10, 2 );

	/**
	 * Allow plugins to add custom
	 *
	 * @since  1.0.0
	 */
	do_action( 'cp_doc_buttons' );

	$content = wp_kses_post( cp_doc_to_edit_get_content() );

	// We need to come back to main site for the WP Editor.
	restore_current_blog();

	wp_editor( $content, 'cp-doc-content', $editor_args );

	// Switch back to the displayed site.
	switch_to_blog( cp_get_displayed_site_id() );

	remove_filter( 'mce_buttons',          'cp_doc_editor_button_filter', 10, 2 );
	remove_filter( 'tiny_mce_before_init', 'cp_doc_editor_before_init',   10, 2 );
}

	/**
	 * Get the Doc's content to edit.
	 *
	 * @since  1.0.0
	 *
	 * @return string The Doc's content to edit.
	 */
	function cp_doc_to_edit_get_content() {
		$doc     = cp_get_doc_to_edit();
		$content = '';

		if ( ! empty( $doc['post_content'] ) ) {
			$content = $doc['post_content'];
		}

		/**
		 * Filter here to edit the doc's content to edit.
		 *
		 * @since  1.0.0
		 *
		 * @param  string $content The doc's content to edit.
		 * @param  array  $doc     The doc parameters.
		 */
		return apply_filters( 'cp_doc_to_edit_get_content', $content, $doc );
	}

/**
 * Output the content label.
 *
 * @since  1.0.0
 *
 * @return string HTML Output.
 */
function cp_doc_to_edit_content_label() {
	echo esc_html( apply_filters( 'cp_doc_to_edit_content_label', __( 'Contenu de la ressource', 'clusterpress-doc' ) ) );
}

/**
 * Output the available chapters.
 *
 * @since  1.0.0
 *
 * @return string HTML Output.
 */
function cp_doc_to_edit_chapters() {
	$output = cp_doc_to_edit_get_chapters();

	if ( $output ) {
		echo $output;
	} else {
		$message = __( 'Aucun chapitre n\'est disponible pour le moment.', 'clusterpress-doc' );

		clusterpress()->feedbacks->add_feedback(
			'chapters[empty]',
			/**
			 * Use this filter if you wish to edit the no extensions message
			 *
			 * @since 1.0.0
			 *
			 * @param string $message The user feedback to inform no extensions were found.
			 */
			apply_filters( 'cp_doc_to_edit_chapters_feedback', $message ),
			'info'
		);

		cp_get_template_part( 'assets/feedbacks' );
	}
}

	/**
	 * Get the available chapters.
	 *
	 * @since  1.0.0
	 *
	 * @return string The Doc's chapters checklist output.
	 */
	function cp_doc_to_edit_get_chapters() {
		$doc  = cp_get_doc_to_edit();
		$args = array( 'taxonomy' => 'cp_chapter' );

		if ( ! empty( $doc['ID'] ) ) {
			$args['object_id'] = (int) $doc['ID'];
		}

		if ( ! empty( $_POST['tax_input']['cp_chapter'] ) ) {
			$args['selected_cats'] = wp_parse_id_list( $_POST['tax_input']['cp_chapter'] );
		}

		$checklist = cp_get_terms_checklist( $args );

		if ( ! $checklist ) {
			return false;
		}

		/**
		 * Filter here to edit The Doc's chapters checklist output.
		 *
		 * @since  1.0.0
		 *
		 * @param string $value The Doc's chapters checklist output.
		 * @param array  $doc   The doc parameters.
		 */
		return apply_filters( 'cp_doc_to_edit_get_chapters', sprintf(
			'<input type="hidden" name="tax_input[%1$s][]" value="0" />
			<ul id="%1$s_checklist" data-wp-lists="list:%1$s" class="categorychecklist form-no-clear">
				%2$s
			</ul>',
			'cp_chapter',
			$checklist
		), $doc );
	}

/**
 * Output the Doc's status to edit.
 *
 * @since  1.0.0
 *
 * @return string HTML Output.
 */
function cp_doc_to_edit_status() {
	echo join( "\n", cp_doc_to_edit_get_status() );
}

	/**
	 * Get the Doc's status to edit.
	 *
	 * @since  1.0.0
	 *
	 * @return string The Doc's status to edit.
	 */
	function cp_doc_to_edit_get_status() {
		$doc    = cp_get_doc_to_edit();
		$status = 'draft';

		$post_status = cp_doc_get_editable_stati( 'edit' );

		if ( ! empty( $doc['post_status'] ) ) {
			$status = sanitize_key( $doc['post_status'] );
		}

		$options = array();
		foreach ( $post_status as $ks => $vs ) {
			$options[] = sprintf( '<option value="%1$s"%2$s>%3$s</option>',
				esc_attr( $ks ),
				selected( $status, $ks, false ),
				esc_html( $vs )
			);
		}

		/**
		 * Filter here to edit the doc's status to edit.
		 *
		 * @since  1.0.0
		 *
		 * @param  array  $options The doc's status options to edit.
		 * @param  array  $doc     The doc parameters.
		 * @param  string $status  The current status of the Doc.
		 */
		return apply_filters( 'cp_doc_to_edit_get_status', $options, $doc, $status );
	}

/**
 * Output the status label.
 *
 * @since  1.0.0
 *
 * @return string HTML Output.
 */
function cp_doc_to_edit_status_label() {
	echo esc_html( apply_filters( 'cp_doc_to_edit_status_label', __( 'Etat de la ressource', 'clusterpress-doc' ) ) );
}

/**
 * Output the submit button and the nonce field.
 *
 * @since 1.0.0
 *
 * @return string HTML Output.
 */
function cp_doc_to_edit_submit() {
	$doc          = cp_get_doc_to_edit();
	$submit_text  = __( 'Enregistrer', 'clusterpress-doc' );
	$hidden_field = '';

	if ( ! empty( $doc['ID'] ) ) {
		$hidden_field = sprintf( '<input type="hidden" value="%1$d" name="cp_doc[ID]" id="cp_doc_id" data-cp-site="%2$d">',
			$doc['ID'],
			get_current_blog_id()
		);

		$submit_text  = __( 'Mettre à jour', 'clusterpress-doc' );
	}

	printf( '
		<input class="button" type="submit" name="cp_doc[submit]" value="%1$s" id="submit">
		%2$s
		',
		esc_attr( $submit_text ),
		$hidden_field
	);

	wp_nonce_field( 'cp_doc_save_doc' );
}
