<?php
/**
 * Doc's Settings.
 *
 * @package ClusterPress Doc\doc
 * @subpackage settings
 *
 * @since 1.0.0
 */

// Exit if accessed directly
defined( 'ABSPATH' ) || exit;

/**
 * Headings settings field callback.
 *
 * @since  1.0.0
 *
 * @return string HTML Output.
 */
function cp_doc_headings_settings() {
	$headings        = array_keys( cp_doc_get_available_editor_headings() );
	$active_headings = array_flip( cp_doc_get_headings() );
	?>
	<ul>
		<?php foreach( $headings as $heading ):?>
			<li>
				<label for="clusterpress-heading-cb-<?php echo esc_attr( $heading ); ?>">
					<input type="checkbox" class="checkbox" id="clusterpress-heading-cb-<?php echo esc_attr( $heading ); ?>" value="<?php echo esc_attr( $heading ); ?>" name="clusterpress_doc_headings[]" <?php checked( isset( $active_headings[ $heading ] ) ); ?>>
					<?php echo esc_html( ucfirst( $heading ) ); ?>
				</label>
			</li>
		<?php endforeach;?>
	</ul>
	<?php
}

/**
 * Headings settings sanitization callback.
 *
 * @since  1.0.0
 *
 * @return array Active headings.
 */
function cp_doc_sanitize_headings( $option ) {
	return array_map( 'sanitize_key', (array) $option );
}

/**
 * Back to top settings field callback.
 *
 * @since  1.0.0
 *
 * @return string HTML Output.
 */
function cp_doc_back_to_tops_settings() {
	?>
	<input name="clusterpress_doc_back_to_tops" id="clusterpress-doc-back-to-tops" type="checkbox" value="1" <?php checked( cp_doc_is_backtotops_enabled() ); ?> />
	<label for="clusterpress-doc-back-to-tops"><?php esc_html_e( 'Ajouter des liens de retour en haut de la page avant chaque titre.', 'clusterpress-doc' ); ?></label>
	<?php
}

/**
 * Chapter checkboxes behavior settings field callback.
 *
 * @since  1.0.0
 *
 * @return string HTML Output.
 */
function cp_doc_chapter_checkboxes_settings() {
	?>
	<input name="clusterpress_doc_chapter_checkboxes" id="clusterpress-doc-chapter-checkboxes" type="checkbox" value="1" <?php checked( cp_doc_is_multi_chapters_enabled() ); ?> />
	<label for="clusterpress-doc-chapter-checkboxes"><?php esc_html_e( 'Limiter la sélection des cases à cocher à une seule.', 'clusterpress-doc' ); ?></label>
	<?php
}
