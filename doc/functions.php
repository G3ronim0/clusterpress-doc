<?php
/**
 * Doc's Functions.
 *
 * @package ClusterPress Doc\doc
 * @subpackage functions
 *
 * @since 1.0.0
 */

// Exit if accessed directly
defined( 'ABSPATH' ) || exit;

/**
 * Get The Addon version.
 *
 * @since 1.0.0
 *
 * @return string The Addon version.
 */
function cp_doc_get_addon_version() {
	return clusterpress()->doc->version;
}

/**
 * Get The Addon path data (url & dir).
 *
 * @since 1.0.0
 *
 * @return array The Addon path data.
 */
function cp_doc_get_addon_path_data() {
	return clusterpress()->doc->path_data;
}

/**
 * Get the addon's directory.
 *
 * @since  1.0.0
 *
 * @return string The addon's directory.
 */
function cp_doc_get_plugin_dir() {
	return clusterpress()->doc->path_data['dir'];
}

/**
 * Get the addon's url.
 *
 * @since  1.0.0
 *
 * @return string The addon's url.
 */
function cp_doc_get_plugin_url() {
	return clusterpress()->doc->path_data['url'];
}

/**
 * Get The Post Type archive slug.
 *
 * @since 1.0.0
 *
 * @return string The Post Type archive slug.
 */
function cp_doc_get_post_type_archive_slug() {
	return apply_filters( 'cp_doc_get_post_type_archive_slug', 'documentation' );
}

/**
 * Get The Post Type archive title.
 *
 * @since 1.0.0
 *
 * @return string The Post Type archive title.
 */
function cp_doc_get_post_type_archive_title() {
	return apply_filters( 'cp_doc_get_post_type_archive_title', __( 'Documentation', 'clusterpress-doc' ) );
}

/**
 * Get The Post Type slug.
 *
 * @since 1.0.0
 *
 * @return string The Post Type slug.
 */
function cp_doc_get_post_type_slug() {
	return apply_filters( 'cp_doc_get_post_type_slug', trailingslashit( cp_doc_get_post_type_archive_slug() ) . clusterpress()->doc->slug );
}

/**
 * Get The Post Type chapter slug.
 *
 * @since 1.0.0
 *
 * @return string The Post Type chapter slug.
 */
function cp_doc_get_chapter_slug() {
	$chapter_slug = apply_filters( 'cp_doc_get_chapter_slug', 'chapitre' );

	return apply_filters( 'cp_doc_get_post_type_chapter_slug', trailingslashit( cp_doc_get_post_type_archive_slug() ) . $chapter_slug );
}

/**
 * Conditional tag checking a Doc archive page is displayed.
 *
 * @since  1.0.0
 *
 * @return bool True if it's a Doc archive page. False otherwise.
 */
function cp_is_docs() {
	return is_clusterpress() && cp_is_current_archive( cp_doc_get_post_type_archive_slug() );
}

/**
 * Get the displayed doc item.
 *
 * @since  1.0.0
 *
 * @return WP_Post The displayed doc item.
 */
function cp_doc_displayed_item() {
	$doc = null;
	$cp  = clusterpress();

	if ( ! empty( $cp->cluster->displayed_object->post_type ) && 'cp_doc' === $cp->cluster->displayed_object->post_type ) {
		$doc = $cp->cluster->displayed_object;
	}

	return $doc;
}

/**
 * Get the displayed doc item ID.
 *
 * @since  1.0.0
 *
 * @return int The displayed doc item ID.
 */
function cp_doc_get_displayed_item_id() {
	$doc = cp_doc_displayed_item();

	if ( ! empty( $doc->ID ) ) {
		return (int) $doc->ID;
	}

	return false;
}

/**
 * Conditional tag checking a Doc single page is displayed.
 *
 * @since  1.0.0
 *
 * @return bool True if it's a Doc single page. False otherwise.
 */
function cp_is_doc() {
	return is_clusterpress() && cp_doc_displayed_item();
}

/**
 * Conditional tag checking a Doc single page is previewed.
 *
 * @since  1.0.0
 *
 * @return bool True if a Doc single page is previewed. False otherwise.
 */
function cp_is_doc_preview() {
	return cp_is_doc() && ! empty( $_GET['preview'] );
}

/**
 * Conditional tag checking a Doc site's discovery page is displayed.
 *
 * @since  1.0.0
 *
 * @return bool True if it's a Doc site's discovery page. False otherwise.
 */
function cp_is_site_doc() {
	return cp_is_site() && cp_doc_get_post_type_archive_slug() === cp_current_action();
}

/**
 * Get The Post Type capabilities.
 *
 * @since 1.0.0
 *
 * @return array The Post Type capabilities.
 */
function cp_doc_get_post_type_caps() {
	return apply_filters( 'cp_doc_get_post_type_caps', array (
		'edit_post'              => 'edit_cp_doc',
		'read_post'              => 'read_cp_doc',
		'delete_post'            => 'delete_cp_doc',
		'edit_posts'             => 'edit_cp_docs',
		'edit_others_posts'      => 'edit_others_cp_docs',
		'publish_posts'          => 'publish_cp_docs',
		'read_private_posts'     => 'read_private_cp_docs',
		'delete_posts'           => 'delete_cp_docs',
		'delete_private_posts'   => 'delete_private_cp_docs',
		'delete_published_posts' => 'delete_published_cp_docs',
		'delete_others_posts'    => 'delete_others_cp_docs',
		'edit_private_posts'     => 'edit_private_cp_docs',
		'edit_published_posts'   => 'edit_published_cp_docs',
	) );
}

/**
 * Get The Post Type labels.
 *
 * @since 1.0.0
 *
 * @return array The Post Type labels.
 */
function cp_doc_get_post_type_labels() {
	return apply_filters( 'cpd_get_post_type_labels', array( 'labels' => array(
		'name'                  => __( 'Docs',                               'clusterpress-doc' ),
		'menu_name'             => _x( 'Documentation', 'Main Plugin menu',  'clusterpress-doc' ),
		'all_items'             => __( 'Tous les docs',                      'clusterpress-doc' ),
		'singular_name'         => __( 'Doc',                                'clusterpress-doc' ),
		'add_new'               => __( 'Ajouter un doc',                     'clusterpress-doc' ),
		'add_new_item'          => __( 'Ajouter doc',                        'clusterpress-doc' ),
		'edit_item'             => __( 'Modifier le doc',                    'clusterpress-doc' ),
		'new_item'              => __( 'Nouveau doc',                        'clusterpress-doc' ),
		'view_item'             => __( 'Afficher le doc',                    'clusterpress-doc' ),
		'search_items'          => __( 'Rechercher un doc',                  'clusterpress-doc' ),
		'not_found'             => __( 'Aucun doc trouvé',                   'clusterpress-doc' ),
		'not_found_in_trash'    => __( 'Aucun doc trouvé dans la corbeille', 'clusterpress-doc' ),
		'insert_into_item'      => __( 'Insérer dans le doc',                'clusterpress-doc' ),
		'uploaded_to_this_item' => __( 'Attaché à ce doc',                   'clusterpress-doc' ),
		'filter_items_list'     => __( 'Filtrer les docs',                   'clusterpress-doc' ),
		'items_list_navigation' => __( 'Navigation de la liste des docs',    'clusterpress-doc' ),
		'items_list'            => __( 'Liste des docs',                     'clusterpress-doc' ),
	) ) );
}

/**
 * Get The Post Type arguments.
 *
 * @since 1.0.0
 *
 * @return array The Post Type arguments.
 */
function cp_doc_get_post_type_args() {
	return apply_filters( 'cpd_get_post_type_args', array(
		'public'              => true,
		'query_var'           => 'cp_doc',
		'rewrite'             => array(
			'slug'            => cp_doc_get_post_type_slug(),
			'with_front'      => false
		),
		'has_archive'         => cp_doc_get_post_type_archive_slug(),
		'exclude_from_search' => true,
		'show_in_nav_menus'   => false,
		'show_in_admin_bar'   => false,
		'menu_icon'           => plugins_url( 'img/doc-menu.svg', plugin_basename( dirname( __FILE__ ) ) ),
		'supports'            => array( 'title', 'editor', 'author', 'revisions' ),
		'taxonomies'          => array( 'cp_chapter' ),
		'capability_type'     => array( 'cp_doc', 'cp_docs' ),
		'capabilities'        => cpp_get_post_type_caps(),
		'delete_with_user'    => false,
		'can_export'          => true,
	) );
}

/**
 * Register the Doc Post Type.
 *
 * @since 1.0.0.
 */
function cp_doc_register_post_type() {
	register_post_type( 'cp_doc', array_merge(
		cp_doc_get_post_type_labels(),
		cp_doc_get_post_type_args()
	) );
}

/**
 * Get The "chapter" Taxonomy capabilities.
 *
 * @since 1.0.0
 *
 * @return array The Taxonomy capabilities.
 */
function cp_doc_get_chapter_caps() {
	return apply_filters( 'cp_doc_get_post_type_chapter_caps', array (
		'manage_terms' => 'manage_cp_doc_chapters',
		'edit_terms'   => 'edit_cp_doc_chapters',
		'delete_terms' => 'delete_cp_doc_chapters',
		'assign_terms' => 'assign_cp_doc_chapters'
	) );
}

/**
 * Get the "chapter" taxonomy labels
 *
 * @since 1.0.0
 *
 * @return array taxonomy labels
 */
function cp_doc_get_chapter_labels() {
	return apply_filters( 'cp_doc_get_chapter_labels', array(
		'labels' => array(
			'name'             => __( 'Chapitres',                     'clusterpress-doc' ),
			'singular_name'    => __( 'Chapitre',                      'clusterpress-doc' ),
			'edit_item'        => __( 'Modifier le chapitre',          'clusterpress-doc' ),
			'update_item'      => __( 'Mettre à jour le chapitre',     'clusterpress-doc' ),
			'add_new_item'     => __( 'Ajouter un nouveau chapitre',   'clusterpress-doc' ),
			'new_item_name'    => __( 'Nouveau nom de chapitre',       'clusterpress-doc' ),
			'all_items'        => __( 'Tous les chapitres',            'clusterpress-doc' ),
			'search_items'     => __( 'Rechercher dans les chapitres', 'clusterpress-doc' ),
			'parent_item'      => __( 'Chapitre parent',               'clusterpress-doc' ),
			'parent_item_colon'=> __( 'Chapitre parent:',              'clusterpress-doc' ),
		)
	) );
}

/**
 * Gets the "chapter" taxonomy arguments
 *
 * @since 1.0.0
 *
 * @return array taxonomy arguments
 */
function cp_doc_get_chapter_args() {
	return apply_filters( 'cp_doc_get_chapter_args', array(
		'rewrite'               => array(
			'slug'              => cp_doc_get_chapter_slug(),
			'with_front'        => false,
			'hierarchical'      => true,
		),
		'capabilities'          => cp_doc_get_chapter_caps(),
		'update_count_callback' => '_update_post_term_count',
		'query_var'             => 'cp_chapter',
		'hierarchical'          => true,
		'show_in_nav_menus'     => false,
		'public'                => true,
		'show_tagcloud'         => false,
	) );
}

/**
 * Register the "chapter" Taxonomy.
 *
 * @since 1.0.0.
 */
function cp_doc_register_taxonomy() {
	register_taxonomy(
		'cp_chapter',
		'cp_doc',
		array_merge(
			cp_doc_get_chapter_labels(),
			cp_doc_get_chapter_args()
		)
	);
}

/**
 * Get the Chapters according to the context.
 *
 * @since 1.0.0
 *
 * @param array $arg an array of arguments {
 *    @type bool $get_object_ids Whether to fetch object ids for each term.
 *
 *    @see get_terms() for the rest of the avalaible arguments.
 * }
 * @return array The list of found terms.
 */
function cp_doc_get_chapters( $args = array() ) {
	$r = wp_parse_args( $args, array(
		'taxonomy'   => 'cp_chapter',
		'orderby'    => 'name',
		'order'      => 'ASC',
		'hide_empty' => 0,
	) );

	$chapters = get_terms( $r );

	foreach ( $chapters as $kc => $chapter ) {
		$chapters[ $kc ]->url = get_term_link( $chapter, 'cp_chapter' );
	}

	return $chapters;
}

/**
 * Add anchors to the titles into the given content.
 *
 * @since  1.0.0
 *
 * @param  string  $content The content to add anchors to.
 * @param  WP_Post $doc     The Doc object.
 * @return string           The content of the doc.
 */
function cp_doc_toc_content( $content = '', $doc = null ) {
	if ( empty( $doc->ID ) ) {
		return $content;
	}

	$doc->toc = new CP_Doc_Toc( $content );

	return $doc->toc->content;
}

/**
 * Enqueue doc styles.
 *
 * @since 1.0.0
 *
 * @param string $min The minified suffix for the style.
 */
function cp_doc_enqueue_cssjs( $min = '' ) {
	if ( ! cp_is_doc() && ! cp_is_docs() && ! cp_is_site_doc() ) {
		return;
	}

	add_filter( 'cp_template_stack', 'cp_doc_set_template_stack_entries', 20, 1 );

	wp_enqueue_style(
		'clusterpress-doc',
		cp_get_template_asset( 'css/doc' . $min, 'css', cp_doc_get_addon_path_data() ),
		array( 'clusterpress' ),
		cp_doc_get_addon_version()
	);

	remove_filter( 'cp_template_stack', 'cp_doc_set_template_stack_entries', 20, 1 );

	if ( cp_is_site_doc() ) {
		wp_enqueue_script( 'clusterpress-doc' );
	}

	if ( cp_is_docs() ) {
		wp_add_inline_script( 'clusterpress-common', cp_doc_chapters_none_fix() );
	}
}
add_action( 'cp_enqueue_cssjs_clusterpress', 'cp_doc_enqueue_cssjs', 10, 1 );

/**
 * Is a search performed ?
 *
 * @since  1.0.0
 *
 * @return bool True if a search is performed. False otherwise.
 */
function cp_doc_is_search() {
	$filters = cp_get_filter_var();

	return ! empty( $filters['cp_search'] );
}

/**
 * Get the Documentation site's discovery URL for a given site.
 *
 * @since 1.0.0
 *
 * @param  WP_Site $site The Site object. Optional. Default to displayed site.
 * @return string        The url to the documentation site's discovery page
 */
function cp_doc_get_site_url( WP_Site $site = null ) {
	if ( ! cp_cluster_is_enabled( 'site' ) ) {
		return false;
	}

	if ( empty( $site->blog_id ) ) {
		$site = cp_displayed_site();
	}

	return cp_site_get_url( array(
		'slug'       => cp_doc_get_post_type_archive_slug(),
		'rewrite_id' => 'cp_site_doc'
	), $site );
}

/**
 * Get the available/one of the site's discovery page doc actions.
 *
 * @since 1.0.0
 *
 * @param  string       $type  'all' to get all actions, the type to get a specific one.
 * @return array|string         All available actions, or a specific one.
 */
function cp_doc_get_site_action( $type = '' ) {
	/**
	 * Filter here to register your custom actions.
	 *
	 * @since 1.0.0
	 *
	 * @param array $value The list of actions.
	 */
	$actions = apply_filters( 'cp_doc_get_site_action', array(
		'add'   => _x( 'nouveau',   'Action used into urls', 'clusterpress-doc' ),
		'edit'  => _x( 'modifier',  'Action used into urls', 'clusterpress-doc' ),
		'trash' => _x( 'supprimer', 'Action used into urls', 'clusterpress-doc' ),
	) );

	if ( 'all' === $type ) {
		return $actions;
	}

	if ( ! isset( $actions[ $type ] ) ) {
		return false;
	}

	return sanitize_key( $actions[ $type ] );
}

/**
 * Conditional tag to check if the doc form is displayed.
 *
 * @since  1.0.0
 *
 * @return bool True if the doc form is displayed. False otherwise.
 */
function cp_doc_is_form() {
	$actions = cp_doc_get_site_action( 'all' );

	return cp_is_site_doc() && ( isset( $_GET[ $actions['add'] ] ) || isset( $_GET[ $actions['edit'] ] ) );
}

/**
 * Conditional tag to check if the doc new form is displayed.
 *
 * @since  1.0.0
 *
 * @return bool True if the doc new form is displayed. False otherwise.
 */
function cp_doc_is_form_new() {
	$action = cp_doc_get_site_action( 'add' );

	return cp_is_site_doc() && isset( $_GET[ $action ] );
}

/**
 * Conditional tag to check if the doc edit form is displayed.
 *
 * @since  1.0.0
 *
 * @return bool True if the doc edit form is displayed. False otherwise.
 */
function cp_doc_is_form_edit() {
	$action = cp_doc_get_site_action( 'edit' );

	return cp_is_site_doc() && isset( $_GET[ $action ] );
}

/**
 * Get the link to add a new doc to the given site
 *
 * @since 1.0.0
 *
 * @param  WP_Site $site The Site object. Optional. Default to displayed site.
 * @return string        The url to add a new doc from the site's discovery page.
 */
function cp_doc_get_add_link( WP_Site $site = null ) {
	return add_query_arg( cp_doc_get_site_action( 'add' ), 1, cp_doc_get_site_url( $site ) );
}

/**
 * Get the available Post Stati for the Doc post type.
 *
 * @since 1.0.0
 *
 * @param  string $context Whether we need the stati for an editing or a listing context.
 * @return array  $stati   The list of available post stati.
 */
function cp_doc_get_editable_stati( $context = 'list' ) {
	$stati = array_diff_key( get_post_statuses(), array( 'pending' => false, 'private' => false ) );

	if ( 'list' === $context ) {
		if ( ! current_user_can( 'publish_cp_docs' ) ) {
			unset( $stati['draft'] );
		}

		return array_keys( $stati );
	}

	return $stati;
}

/**
 * Get the list of avaialable block elements for the WP Editor.
 *
 * @since 1.0.0
 *
 * @return array The list of available blocks for the WP Editor.
 */
function cp_doc_get_available_editor_headings() {
	return apply_filters( 'cp_doc_get_available_editor_headings', array(
		'h1'  => 'Heading 1',
		'h2'  => 'Heading 2',
		'h3'  => 'Heading 3',
		'h4'  => 'Heading 4',
		'h5'  => 'Heading 5',
		'h6'  => 'Heading 6',
	) );
}

/**
 * Get the active headings.
 *
 * @since 1.0.0
 *
 * @return array The active heading tags.
 */
function cp_doc_get_headings() {
	return (array) get_network_option( 0, 'clusterpress_doc_headings', array( 'h1', 'h2', 'h3', 'h4', 'h5', 'h6' ) );
}

/**
 * Are back to tops enabled ?
 *
 * @since 1.0.0
 *
 * @return bool True if back to tops are enabled. False otherwise.
 */
function cp_doc_is_backtotops_enabled() {
	return (bool) get_network_option( 0, 'clusterpress_doc_back_to_tops', false );
}

/**
 * Can more than one chapter checkboxes be selected ?
 *
 * @since 1.0.0
 *
 * @return bool True if more than one chapter checkboxes can be selected. False otherwise.
 */
function cp_doc_is_multi_chapters_enabled() {
	return (bool) get_network_option( 0, 'clusterpress_doc_chapter_checkboxes', false );
}

/**
 * Checks if a doc is currently edited by another user.
 *
 * @since  1.0.0
 *
 * @param  int       $doc_id The Doc's ID
 * @return false|int         False or the user id currently editing the doc.
 */
function cp_doc_check_post_lock( $doc_id = 0 ) {
	if ( empty( $doc_id ) ) {
		return false;
	}

	if ( ! function_exists( 'wp_check_post_lock' ) ) {
		require_once( ABSPATH . '/wp-admin/includes/post.php' );
	}

	return wp_check_post_lock( $doc_id );
}

/**
 * Eventually Lock a post if a user is editing the doc.
 *
 * @since  1.0.0
 *
 * @param  int       $doc_id  The Doc's ID.
 * @param  int       $site_id The site's ID.
 * @return false|int          False or the user id currently editing the doc.
 */
function cp_doc_lock_post( $doc_id = 0, $site_id = 0 ) {
	if ( empty( $doc_id ) || empty( $site_id ) ) {
		return false;
	}

	switch_to_blog( $site_id );

	$user_id = cp_doc_check_post_lock( $doc_id );

	if ( ! $user_id ) {
		wp_set_post_lock( $doc_id );
	}

	restore_current_blog();

	return $user_id;
}
